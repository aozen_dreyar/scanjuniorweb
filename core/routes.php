<?php
// Define routes to be added to the router instance (by request type).
$router->get('', 'ProductController@list');
$router->get('add-product', 'ProductController@create');
$router->post('add-product', 'ProductController@store');
$router->post('delete-product', 'ProductController@delete');
