<?php require('partials/head.php'); ?>

<div class="container">
    <h4>Product Add</h4>

    <form action="" method="post" id="product_form">

        <div class="d-flex justify-content-end">
            <button type="submit" class="btn btn-success">Save</button>
            <a href="/" class="btn btn-primary">Cancel</a>
        </div>
        <div class="mb-3">
            <label for="sku" class="form-label">SKU</label>
            <input type="text" class="form-control" id="sku" name="sku" required>
            <p id="sku-error" class="text-danger"></p>
        </div>
        <div class="mb-3">
            <label for="name" class="form-label">Name</label>
            <input type="text" class="form-control" id="name" name="name" required>
            <p id="name-error" class="text-danger"></p>
        </div>
        <div class="mb-3">
            <label for="price" class="form-label">Price (&dollar;)</label>
            <input type="number" step="0.01" class="form-control" id="price" name="price" required>
            <p id="price-error" class="text-danger"></p>
        </div>
        <div class="mb-3">
            <label for="productType" class="form-label">Type Switcher</label>
            <select class="form-select" aria-label="Product Type" id="productType" name="productType" required>
                <option selected></option>
                <option value="DVD">DVD</option>
                <option value="Furniture">Furniture</option>
                <option value="Book">Book</option>
            </select>
            <p id="productType-error" class="text-danger"></p>
        </div>
        <div class="card d-none" id="DVD">
            <div class="card-body">
                <div class="mb-3">
                    <label for="size" class="form-label">Size (MB)</label>
                    <input type="number" step="0.01" class="form-control" id="size" name="size">
                    <p id="size-error" class="text-danger"></p>
                </div>
                <p>Please provide size</p>
            </div>
        </div>
        <div class="card d-none" id="Furniture">
            <div class="card-body">
                <div class="mb-3">
                    <label for="height" class="form-label">Height (cm)</label>
                    <input type="number" step="0.01" class="form-control" id="height" name="height">
                    <p id="height-error" class="text-danger"></p>
                </div>
                <div class="mb-3">
                    <label for="width" class="form-label">Width (cm)</label>
                    <input type="number" step="0.01" class="form-control" id="width" name="width">
                    <p id="width-error" class="text-danger"></p>
                </div>
                <div class="mb-3">
                    <label for="length" class="form-label">Length (cm)</label>
                    <input type="number" step="0.01" class="form-control" id="length" name="length">
                    <p id="length-error" class="text-danger"></p>
                </div>
                <p>Please provide dimensions in HxWxL format</p>
            </div>
        </div>
        <div class="card d-none" id="Book">
            <div class="card-body">
                <div class="mb-3">
                    <label for="weight" class="form-label">Weight (KG)</label>
                    <input type="number" step="0.01" class="form-control" id="weight" name="weight">
                    <p id="weight-error" class="text-danger"></p>
                </div>
                <p>Please provide weight</p>
            </div>
        </div>
    </form>

</div>

<script>
    // start processing only when dom ready
    document.addEventListener("DOMContentLoaded", function() {
        const form = document.getElementById('product_form');
        const switcher = document.getElementById('productType');
        const fieldIds = [
            'sku',
            'name',
            'price',
            'productType',
            'size',
            'weight',
            'height',
            'width',
            'length',
        ];

        form.addEventListener('submit', function (e) {
            // prevent form from submitting normally
            e.preventDefault();

            // send request via fetch API
            fetch('/add-product', {
                method: 'POST',
                body: getData(),// get data to send
            })
                .then(response => response.json())// get JSON response
                .then(data => {
                    // check for any errors
                    if (data.errors !== undefined) {
                        handleErrors(data.errors);
                        return
                    }
                    // else if successful, redirect back to list page
                    document.location.href = '/';
                })
                .catch((error) => {
                    console.error('Error:', error);
                });
        });

        // add event listener for type dropdown changing
        switcher.addEventListener('change', changeType);

        // this is a separate function, so we can call it on page load as well
        // (some browsers keep form state when you refresh, so we need to make sure the state makes sense)
        function changeType() {
            const selectedType = switcher.options[switcher.selectedIndex].value;
            const types = ['DVD', 'Furniture', 'Book'];

            types.forEach( (type) => {
                let attributes = document.getElementById(type);
                if (type === selectedType) {
                    attributes.classList.remove('d-none');
                    return;
                }
                attributes.classList.add('d-none');
            });
        }

        // call the changeType method to check what should be hidden/shown
        changeType();

        // gets all the data we need to send over fetch, as form data rather than JSON so that we can use $_POST instead of input() method
        function getData() {
            const formData = new FormData();
            fieldIds.forEach((id) => {
                // task stated that ID should be productType for this field but in backend we use type
                // so this check is to change that in the sent data
                let field = id === 'productType' ? 'type' : id;
                formData.append(field, document.getElementById(id).value);
            });

            return formData;
        }

        // handle errors, loop the inputs and add text to the relevant errors (or clear out any that aren't errored)
        function handleErrors(errors) {
            fieldIds.forEach((id) => {
                let feedback = document.getElementById(`${id}-error`);
                if (feedback === undefined || feedback === null) {
                    return;
                }
                if (errors[id] ===  undefined) {
                    feedback.innerHTML = '';
                    return
                }
                feedback.innerHTML = errors[id];
            });
        }

    });
</script>

<?php require('partials/footer.php'); ?>
