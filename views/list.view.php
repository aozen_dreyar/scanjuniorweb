<?php require('partials/head.php'); ?>

<div class="container">

    <h1>Product List</h1>
    <form action="/delete-product" method="post">
        <div class="d-flex justify-content-end">
            <a href="/add-product" class="btn btn-primary">Add</a>
            <button id="delete-product-btn" type="submit" class="btn btn-danger">Mass Delete</button>
        </div>

        <div class="row">
            <?php foreach ($products as $product): ?>
                <div class="col-sm-3">
                    <div class="card">
                        <div class="card-body">
                            <input type="checkbox" class="delete-checkbox" name="delete_ids[]" value="<?= $product->getId() ?>">
                            <h5 class="card-title text-center"><?= $product->getSku() ?></h5>
                            <p class="card-text text-center">
                                <?= $product->getName() ?>
                                <br>
                                <?= $product->getPrice() ?> &dollar;
                                <br>
                                <?= $product->getAttributesDisplayData() ?>
                            </p>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </form>

</div>

<?php require('partials/footer.php'); ?>
