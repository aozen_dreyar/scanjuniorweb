create table products
(
    id     int auto_increment
        primary key,
    sku    varchar(20)    not null,
    name   varchar(50)    not null,
    price  decimal(15, 2) not null,
    type   varchar(10)    not null,
    size   float          null,
    weight float          null,
    height float          null,
    width  float          null,
    length float          null,
    constraint products_sku_uindex
        unique (sku)
);

