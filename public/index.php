<?php
// Sole purpose is to serve as application's entry point and set up the project.

// Auto-loader
spl_autoload_register(static function ($class) {
    # Split class/namespace into array
    $class = explode('\\', $class);
    # If first part of namespace is App, replace with src
    if ($class[0] === 'App') {
        $class[0] = 'app';
    }
    array_unshift($class, __DIR__, '..');
    # Join array by directory separator and load file
    require_once implode(DIRECTORY_SEPARATOR, $class) . '.php';
});

require '../core/bootstrap.php';

// PHP 7 allows you to consolidate namespacing where files share the same path:
use App\Core\{Router, Request};

// Load routes into a router instance, and then direct traffic to
// the Controllers associated with a URI based on the request method
// (NOTE: It is not necessary to preface this with a "require" keyword
// because the Router.direct() method will call Router.callAction(),
// which will create a new instance of a necessary controller):
Router::load('../core/routes.php')
    ->direct(Request::uri(), Request::method());
