<?php

namespace App\Controllers;

use App\Models\Product;
use ReflectionClass;

class ProductController
{
    public function list()
    {
        $products = Product::getAll();
        // view() is a custom helper function contained in "core/bootstrap.php":
        return view('list', ['products' => $products]);
    }

    public function create()
    {
        return view('create');
    }

    public function store()
    {
        if (!in_array($_POST['type'], ['DVD', 'Book', 'Furniture'])) {
            return view('json', ['json' => [
                'errors' => ['productType' => 'Invalid product type']
            ]]);
        }
        $validationErrors = Product::validate($_POST);
        if (!empty($validationErrors)) {
            return view('json', ['json' => ['errors' => $validationErrors]]);
        }
        $product = (new ReflectionClass("App\\Models\\" . $_POST['type']))->newInstanceArgs(array_filter($_POST));
        $validationErrors = $product->validateAttributes();
        if (!empty($validationErrors)) {
            return view('json', ['json' => ['errors' => $validationErrors]]);
        }
        $product->save();
        // Custom helper function contained in "core/bootstrap.php":
        return view('json', ['json' => ['success' => true]]);
    }

    public function delete()
    {
        $deleteIds = $_POST['delete_ids'] ?? [];
        foreach ($deleteIds as $id) {
            Product::delete($id);
        }
        return redirect('');
    }


}
