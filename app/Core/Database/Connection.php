<?php

namespace App\Core\Database;

use PDO;
Use PDOException;

class Connection
{
    public static function make(array $db): PDO
    {
        try {
            return new PDO(
                "{$db['connection']};dbname={$db['schema']}",
                $db['username'],
                $db['password'],
                $db['options']
            );
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
}

