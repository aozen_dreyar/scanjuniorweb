<?php

namespace App\Models;

use App\Core\App;
use ReflectionClass;

// Abstract so it can't be instantiated
abstract class Product
{
    // Private/protected, so they can't be accessed directly outside the class
    protected ?int $id;
    protected string $sku;
    protected string $name;
    protected float $price;
    protected string $type;

    // Set a default constructor that all products will use
    public function __construct(string $sku, string $name, string $type, float $price, ?int $id = null)
    {
        $this->setId($id);
        $this->setSku($sku);
        $this->setName($name);
        $this->setType($type);
        $this->setPrice($price);
    }

    protected function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSku(): string
    {
        return $this->sku;
    }

    public function setSku(string $sku): void
    {
        $this->sku = $sku;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function save(): void
    {
        $data = [
            'sku' => $this->getSku(),
            'name' => $this->getName(),
            'price' => $this->getPrice(),
            'type' => $this->getType(),
        ];
        $attributesData = $this->getAttributesData();
        $data = array_merge($data, $attributesData);
        App::get('Database')->insert('products', $data);
    }

    // static so they can be called without instantiating the class
    public static function getAll(): array
    {
        $products = App::get('Database')->selectAll('products');
        return self::mapProducts($products);
    }

    public static function findBy($column, $value): array
    {
        $products = App::get('Database')->findBy('products', $column, $value);
        return self::mapProducts($products);
    }

    // maps the SQL select query results to the appropriate class
    protected static function mapProducts(array $products): array
    {
        return array_map(static function($product) {
            try {
                return (new ReflectionClass("App\\Models\\" . $product['type']))->newInstanceArgs(array_filter($product));
            } catch(\Exception $e) {
                return null;
            }
        }, $products);
    }

    // delete by id, static so you don't have to query the DB to get the object first
    public static function delete(int $id)
    {
        return App::get('Database')->delete('products', $id);
    }

    // validates the inputs, checking for empty fields (and duplicate SKU)
    public static function validate(array $data): array
    {
        $errors = [];
        if (is_null($data['sku'])) {
            $errors['sku'] = 'SKU should be filled out';
        } else {
            $existingProduct = self::findBy('sku', $data['sku']);
            if (count($existingProduct) > 0) {
                $errors['sku'] = 'SKU already exists';
            }
        }
        if (is_null($data['name'])) {
            $errors['name'] = 'Name should be filled out';
        }
        if (is_null($data['price'])) {
            $errors['price'] = 'Price should be filled out';
        }
        if (is_null($data['type'])) {
            $errors['productType'] = 'Type should be filled out';
        }
        return $errors;
    }

    // Below methods are abstract so that the classes that extend this class have to implement them
    abstract protected function getAttributesData(): array;

    abstract public function validateAttributes(): array;

    abstract public function getAttributesDisplayData(): string;
}