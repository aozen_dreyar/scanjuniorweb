<?php

namespace App\Models;

class Furniture extends Product
{
    private ?float $height;
    private ?float $width;
    private ?float $length;

    public function __construct(string $sku, string $name, string $type, float $price, ?float $height = null, ?float $width = null, ?float $length = null, ?int $id = null)
    {
        parent::__construct($sku, $name, $type, $price, $id);
        $this->setHeight($height);
        $this->setWidth($width);
        $this->setLength($length);
    }

    public function getHeight(): ?float
    {
        return $this->height;
    }

    public function setHeight(?float $height = null): void
    {
        $this->height = $height;
    }

    public function getWidth(): ?float
    {
        return $this->width;
    }

    public function setWidth(?float $width = null): void
    {
        $this->width = $width;
    }

    public function getLength(): ?float
    {
        return $this->length;
    }

    public function setLength(?float $length = null): void
    {
        $this->length = $length;
    }

    public function validateAttributes(): array
    {
        $errors = [];
        if (empty($this->getHeight())) {
            $errors['height'] = 'Height should be filled out';
        }
        if (empty($this->getWidth())) {
            $errors['width'] = 'Width should be filled out';
        }
        if (empty($this->getLength())) {
            $errors['length'] = 'Length should be filled out';
        }
        return $errors;
    }

    protected function getAttributesData(): array
    {
        return [
            'height' => $this->getHeight(),
            'width' => $this->getWidth(),
            'length' => $this->getLength(),
        ];
    }

    public function getAttributesDisplayData(): string
    {
        return "Dimension: {$this->getHeight()}x{$this->getWidth()}x{$this->getLength()}";
    }
}