<?php

namespace App\Models;

class DVD extends Product
{
    private ?float $size;

    public function __construct(string $sku, string $name, string $type, float $price, ?float $size = null, ?int $id = null)
    {
        parent::__construct($sku, $name, $type, $price, $id);
        $this->setSize($size);
    }

    public function getSize(): ?float
    {
        return $this->size;
    }

    public function setSize(?float $size = null): void
    {
        $this->size = $size;
    }

    public function validateAttributes(): array
    {
        $errors = [];
        if (empty($this->getSize())) {
            $errors['size'] = 'Size should be filled out';
        }

        return $errors;
    }

    protected function getAttributesData(): array
    {
        return ['size' => $this->getSize()];
    }

    public function getAttributesDisplayData(): string
    {
        return "Size: {$this->getSize()} MB";
    }
}