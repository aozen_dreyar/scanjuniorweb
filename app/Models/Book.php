<?php

namespace App\Models;

class Book extends Product
{
    private ?float $weight;

    public function __construct(string $sku, string $name, string $type, float $price, ?float $weight = null, ?int $id = null)
    {
        parent::__construct($sku, $name, $type, $price, $id);

        $this->setWeight($weight);
    }

    public function getWeight(): ?float
    {
        return $this->weight;
    }

    public function setWeight(?float $weight = null): void
    {
        $this->weight = $weight;
    }

    public function validateAttributes(): array
    {
        $errors = [];

        if (empty($this->getWeight())) {
            $errors['weight'] = 'Weight should be filled out';
        }

        return $errors;
    }

    protected function getAttributesData(): array
    {
        return ['weight' => $this->getWeight()];
    }

    public function getAttributesDisplayData(): string
    {
        return "Weight: {$this->getWeight()} KG";
    }
}